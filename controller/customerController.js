const db = require('../models')

const Customer = db.customers

//  create customer
const addCustomer = async (req, res) => {
  try {
    const info = {
      id: req.body.id,
      name: req.body.name,
      email: req.body.email  
    }
    const customers = await Customer.create(info)
    res.status(200).send(customers)
  } catch (error) {
    console.log(error)
    res.status(500).send('Can Not Add Customer')
  }
}

// get all customers
const getAllCustomers = async (req, res) => {
  try {
    const customers = await Customer.findAll({})
    res.status(200).send(customers)
  } catch (error) {
    console.log(error)
    res.status(500).send('Can Not Get Customers!')
  }
}

// get a single customer
const getOneCustomer = async (req, res) => {
  try {
    const id = req.params.id
    const customers = await Customer.findOne({ where: { id } })
    res.status(200).send(customers)
  } catch (error) {
    console.log(error)
    res.status(500).send('Can Not Get Customer!')
  }
}

// delete customer by id
const deleteCustomer = async (req, res) => {
  try {
    const id = req.params.id
    await Customer.destroy({ where: { id } })
    res.status(200).send('Customer is deleted !')
  } catch (error) {
    console.log(error)
    res.status(500).send('Can Not Delete!')
  }
}

// update customer
const updateCustomer = async (req, res) => {
  try {
    const id = req.params.id
    const customer = await Customer.update(req.body, { where: { id } })
    res.status(200).send(customer)
  } catch (error) {
    res.status(500).send('Can Not Update')
  }
}
module.exports = {
  addCustomer,
  getAllCustomers,
  getOneCustomer,
  deleteCustomer,
  updateCustomer
}
