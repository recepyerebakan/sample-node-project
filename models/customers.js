// eslint-disable-next-line no-unused-vars
const { Sequelize, DataTypes } = require('sequelize')

module.exports = (sequelize, DataTypes) => {
  const customers = sequelize.define('customers', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING
    },
    email: {
        type: DataTypes.STRING
      },
  })
  return customers
}
