const { Sequelize, DataTypes } = require('sequelize')
const dotenv = require('dotenv').config()

const sequelize = new Sequelize(
  process.env.DB_DATABASE,
  process.env.DB_USERNAME,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT,
    operatorsAliases: false
  }
)

sequelize.authenticate().then(() => {
  console.log('Connected..')
}).catch(err => {
  console.log('Error', err)
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.sequelize.sync({ force: false })
  .then(() => {
    console.log('re-sync done!')
  })

db.customers = require('./customers.js')(sequelize, DataTypes)

module.exports = db