#install bitnami
helm repo add bitnami https://charts.bitnami.com/bitnami

#define a random password
export MYSQL_PASSWORD=pwd123456

#create mysql service, set password and db name
 helm upgrade --install sample-db \  
  --set auth.rootPassword=$MYSQL_PASSWORD,auth.database=customer,auth.password=$MYSQL_PASSWORD \
    bitnami/mysql

echo 'waiting for server to be up'
sleep 240
#Initialize MYSQL DB
echo 'copy sql files into the container'
kubectl cp database/create-table.sql default/sample-db-postgresql-0:/tmp
kubectl cp database/sample-data.sql default/sample-db-postgresql-0:/tmp
echo 'connect and execute the sql commands'

kubectl exec sample-db-postgresql-0 -it -- bash -c "cd tmp &&
PGPASSWORD="$POSTGRES_PASSWORD" psql -U postgres -d foodie -f create-table.sql"

kubectl exec sample-db-postgresql-0 -it -- bash -c "cd tmp &&
PGPASSWORD="$POSTGRES_PASSWORD" psql -U postgres -d foodie -f sample-data.sql"


